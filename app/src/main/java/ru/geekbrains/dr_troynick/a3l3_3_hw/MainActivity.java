package ru.geekbrains.dr_troynick.a3l3_3_hw;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDialog();
    }

    private void initDialog() {
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("title").setMessage("message").setCancelable(false).setNegativeButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog = builder.create();
    }

    public void onClick(View view) {
        alertDialog.show();
    }

}
